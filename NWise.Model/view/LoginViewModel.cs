

using System.ComponentModel.DataAnnotations;

namespace NWise.Model.View
{
    public class SigninViewModel {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
        
}