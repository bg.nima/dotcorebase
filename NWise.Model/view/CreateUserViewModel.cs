using System.ComponentModel.DataAnnotations;
using NWise.Model.Db;

namespace NWise.Model.View
{
    public class CreateUserViewModel : GenericCreateViewModel<User>
    {
        [Required, MaxLength(255)]
        public string FirstName { get; set; }
        [Required, MaxLength(255)]
        public string LastName { get; set; }
        [Required, DataType(DataType.EmailAddress), MaxLength(255)]
        public string Email { get; set; }
        [Required, MaxLength(255)]
        public string Username { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [Required, DataType(DataType.Password), Compare("Password")]
        public string ConfirmPassword { get; set; }
        [Required]
        public long RoleId { get; set; }
    }
}