﻿using System;
using System.Collections.Generic;

namespace NWise.Model.Db
{
    public partial class Permission : IEntity
    {

        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public ICollection<RolePermission> RolePermissions {get;set;}
    }
}
