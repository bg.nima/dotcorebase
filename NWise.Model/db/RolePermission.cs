using System;
using System.Collections.Generic;

namespace NWise.Model.Db
{
    public partial class RolePermission : IEntity
    {
        public long Id {get;set;}
        public long RoleId { get; set; }
        public Role Role { get; set; }
        public long PermissionId { get; set; }
        public Permission Permission { get; set; }
    }
}
