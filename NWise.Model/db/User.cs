﻿using System;
using System.Collections.Generic;

namespace NWise.Model.Db
{
    public partial class User : IEntity
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long RoleId { get; set; }
        public Role Role { get; set; }
        public string ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordExpires { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
