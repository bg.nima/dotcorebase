
using System;

namespace NWise.Model.Db
{
    public interface IEntity
    {
        long Id {get; set;}        
    }
}