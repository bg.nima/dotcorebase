﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NWise.Model.Db;

namespace NWise.Data
{
    public partial class DotNetDbContext : DbContext
    {
        private readonly IConfiguration configuration;

        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }

        // public DotNetDbContext(IConfiguration configuration)
        // {
        //     this.configuration = configuration;
        // }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // optionsBuilder.UseNpgsql(configuration.GetConnectionString("AppDataBase"));
                optionsBuilder.UseNpgsql("Host=localhost;Database=dotnet_db;Username=nima;Password=1234");

                LoggerFactory loggerFactory = new LoggerFactory();
                loggerFactory.AddProvider(new ConsoleLoggerProvider());
                optionsBuilder.UseLoggerFactory(loggerFactory);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Roles");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("timestamptz");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("timestamptz");
            });

            modelBuilder.Entity<Permission>(entity => 
            {
                entity.ToTable("Permissions");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(255);                        
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.ToTable("RolePermissions");

                entity.HasOne(rp => rp.Permission)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(rp => rp.PermissionId);

                entity.HasOne(rp => rp.Role)
                    .WithMany(r => r.RolePermissions)
                    .HasForeignKey(rp => rp.RoleId);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users");
                
                entity.HasIndex(e => e.Email)
                    .HasName("email_index")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("username_index")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("timestamptz");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("firstName");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("lastName");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(511)
                    .HasColumnName("password");

                entity.Property(e => e.ResetPasswordExpires)
                    .HasColumnName("resetPasswordExpires")
                    .HasColumnType("timestamptz");

                entity.Property(e => e.ResetPasswordToken)
                    .HasMaxLength(511)
                    .HasColumnName("resetPasswordToken");

                entity.Property(e => e.RoleId)
                    .IsRequired()
                    .HasColumnName("roleId");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("timestamptz");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("username");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_user_role");
            });
        }
    }
}
