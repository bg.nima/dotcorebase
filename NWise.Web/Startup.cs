﻿using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NWise.Service;

namespace NWise.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(auth => {
                        auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(cfg=>{
                        cfg.RequireHttpsMetadata= false;
                        cfg.SaveToken = true;
                        cfg.TokenValidationParameters = new TokenValidationParameters() {
                            ValidIssuer = Configuration["Tokens:Issuer"],
                            ValidAudience = Configuration["Tokens:Issuer"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]))
                        };
                    });
            services.AddMvc();

            services.ResolveServiceLayerDependancies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            Console.WriteLine(env.WebRootPath);
            app.UseDefaultFiles();
            app.UseStaticFiles();         
            app.UseAuthentication();
            app.UseMvc();
            app.Run(async (context) =>
            {
                Console.WriteLine(context.Request.Headers["X-Requested-With"]);
                var content = await System.IO.File.ReadAllTextAsync("wwwroot/dist/index.html");
                await context.Response.WriteAsync(content);
            });
        }
    }
}
