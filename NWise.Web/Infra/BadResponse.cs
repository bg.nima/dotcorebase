﻿using Microsoft.AspNetCore.Mvc;

namespace NWise.Web.Infra
{
    public class BadResponse : BadRequestObjectResult
    {
        public BadResponse(string message, object data = null) : base(new { success = false, message,  data })
        {
            
        }
    }
}