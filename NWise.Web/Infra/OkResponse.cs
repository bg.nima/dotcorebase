﻿using Microsoft.AspNetCore.Mvc;

namespace NWise.Web.Infra
{
    public class OkResponse : OkObjectResult
    {
        public OkResponse(object value) : base(new {success= true, data= value})
        {
            
        }
    }
}