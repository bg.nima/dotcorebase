'use strict';

var gulp            = require('gulp');
var sass            = require('gulp-sass');
var autoprefixer    = require('gulp-autoprefixer');
var flatten         = require('gulp-flatten');
var rename          = require('gulp-rename');
var runSequence	    = require('run-sequence');
var browserSync     = require('browser-sync').create();
var history 		= require('connect-history-api-fallback');

gulp.task('sass:build', function () {
    return gulp.src('./src/assets/scss/**/*.s*ss')
               .pipe(sass().on('error', sass.logError))
               .pipe(flatten())
               .pipe(autoprefixer(['last 2 versions', 'ie 8', 'ie 9']))
            //    .pipe(rename({ suffix: '-ltr' }))
               .pipe(gulp.dest('./dist/assets/css'))
               .pipe(browserSync.stream());
            //    .pipe(rtlcss())
            //    .pipe(rename(function (path) {
			// 			path.basename = path.basename.replace('-ltr','-rtl');
            //     }))
            //    .pipe(gulp.dest('./dist/assets/css'))
            //    .pipe(browserSync.stream());
});

gulp.task('copy', function () {
    return gulp.src(['./src/**/*.*', '!./src/assets/scss/**/*.*', '!./src/i18n/**/*.*'])
              .pipe(gulp.dest('./dist'));
});


gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./dist",
				middleware: [history()]
        }
    });
    
    gulp.watch('assets/scss/**/*.s*ss', {cwd:'./src'}, ['sass:build']);
    gulp.watch(['**/*.*', '!index.html', '!assets/scss/**/*.*'], {cwd:'./src'})
        .on('change', function (ev) {
            if (ev.type=='changed' || ev.type=='added') {
                gulp.src(ev.path, {base: './src'}).pipe(gulp.dest('./dist', {  }));
            } else if (ev.type=='deleted') {
                var path = ev.path.replace('src/','dist/').replace('src\\','dist\\');
                gulp.src(path).pipe(deleteFile({ deleteMatch: true}));
            }
            browserSync.reload();
        });
});

gulp.task('dev', function() {
	runSequence('copy', 'sass:build', 'serve');
});