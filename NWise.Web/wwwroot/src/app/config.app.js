(function () {
    "use static";

    angular
        .module('nwise')
        .config(appConfig);

    appConfig.$inject = ['configProvider']
    function appConfig(configProvider) {   
        configProvider.setServerBaseUrl('');
    }
})();