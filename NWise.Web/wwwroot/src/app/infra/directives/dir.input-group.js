(function () {
    
    angular
        .module('nwise.infra')
        .component('inputGroup', {
            templateUrl: 'app/infra/directives/dir.input-group.html',
            transclude: true,
            bindings: {
                type: '@',
                floating: '<',
                icon: '@'
            },
            controller: controller
        });
    
    function controller() {
        this.$onInit = function () {
            this.type = this.type || 'form-group';
            console.log(this.floating);
        }
    }
})();