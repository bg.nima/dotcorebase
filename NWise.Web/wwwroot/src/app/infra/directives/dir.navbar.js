(function () {
    "use strict";
    
    angular
        .module('nwise.infra')
        .directive('nwNavbar', nwNavbar);
    
    nwNavbar.$inject = [];
    function nwNavbar() {
        return {
            templateUrl: 'app/infra/directives/dir.navbar.html',
            strict: 'E',
            link: link
        };
        
        function link(scope, element, attr) {
            var $searchBar = $(element).find('.search-bar');
            $.AdminBSB.navbar.activate();
            $.AdminBSB.search.activate($searchBar);


        }
    }
})();