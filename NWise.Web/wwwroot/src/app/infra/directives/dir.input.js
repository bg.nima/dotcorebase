(function () {
    "use strict";
    
    angular
        .module('nwise.infra')
        .directive('nwInput', nwInput);
    
    nwInput.$inject = [];
    function nwInput() {
        return {
            restrict : 'EAC',
            link: link
        };
        
        function link(scope, element, attrs) {
            $.AdminBSB.input.activate(element);
        }
    }
})();