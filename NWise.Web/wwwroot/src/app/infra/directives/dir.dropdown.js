(function () {
     angular
         .module('nwise.infra')
         .directive('nwDropdown', nwDropdown);
     
     nwDropdown.$inject = [];
     function nwDropdown() {
         return {
              restrict: 'EAC',
              link: link
         }
     }
     function link(scope, element, attrs) {
         $.AdminBSB.dropdownMenu.activate(element);
     }
})();