(function () {
    "use strict";
    
    angular
        .module('nwise.infra')
        .directive('nwLoader', nwLoader);
    
    nwLoader.$inject = [];
    function nwLoader() {
        return {
            templateUrl: 'app/infra/directives/dir.loader.html',
            restrict : 'E',
            link: link
        };
        
        function link(scope, element, attrs) {
            
        }
    }
});