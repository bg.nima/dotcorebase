(function () {
    "use strict";

    angular.module('nwise.infra')
        .directive('nwSidebar', nwSidebar);
    
    nwSidebar.$inject = ['authenticationService'];
    function nwSidebar(authenticationService) {
        return {
            link: link,
            templateUrl: 'app/infra/directives/dir.sidebar.html',
            restrict : 'E',
            controllerAs: 'vm',
            controller: ctrl
        };
        
        function link(scope, element, attr) {
            $.AdminBSB.leftSideBar.activate();
            
        }

        function ctrl() {
            var vm = this;

            vm.signout = function() {
                authenticationService.signout();
            }
        }
    }
})();