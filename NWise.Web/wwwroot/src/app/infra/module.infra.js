(function () {
    "use strict";
    
    angular.module('nwise.infra', [
        'ui.router',
        'ngCookies'
    ]);
    
})();