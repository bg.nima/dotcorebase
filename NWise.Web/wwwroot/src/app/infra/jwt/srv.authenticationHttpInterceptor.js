(function () {
    "use strict";
    
    angular
        .module('nwise.infra')
        .service('authenticationHttpInterceptor', authenticationHttpInterceptor);
    
    authenticationHttpInterceptor.$inject = ['$q', 'jwtToken', 'jwtAuthentication'];
    function authenticationHttpInterceptor($q, jwtToken, jwtAuthentication) {
        this.request = function(config) {
            config.headers = config.headers || {};

            // Inject `Authorization` header.
            if (!config.headers.hasOwnProperty('Authorization') && jwtAuthentication.isAuthenticated()) {
                config.headers.Authorization = jwtToken.getAuthorizationHeader();
            }

            return config;
        };
        
        this.responseError = function(rejection) {
            if (!rejection) {
                return $q.reject(rejection);
            }

            // Catch `invalid_request` and `invalid_grant` errors and ensure that the `token` is removed.
            // if (400 === rejection.status && rejection.data &&
            //     ('invalid_request' === rejection.data.error || 'invalid_grant' === rejection.data.error)
            // ) {
            //     jwtToken.removeToken();
            // }

            // Catch `invalid_token` and `unauthorized` errors.
            // The token isn't removed here so it can be refreshed when the `invalid_token` error occurs.
            if (401 === rejection.status) {
                jwtToken.removeToken();
            }

            return $q.reject(rejection);
        }
    }
})();