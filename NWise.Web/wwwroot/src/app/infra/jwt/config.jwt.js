(function () {
    angular
        .module('nwise.infra')
        .config(jwtConfig)
        .run(jwtRun);

    jwtConfig.$inject = ['$httpProvider', '$uiRouterProvider'];
    function jwtConfig($httpProvider, $uiRouterProvider) {
        
        $httpProvider.interceptors.push('authenticationHttpInterceptor');
    }

    jwtRun.$inject = ['jwtAuthentication', '$transitions', '$state'];
    function jwtRun(jwtAuthentication,$transitions,$state) {
        
        $transitions.onBefore({}, function (trans) {
            var to = trans.to();
            if (!to.anonymouse && !jwtAuthentication.isAuthenticated()) {
                console.log('You are not allowed to view this page');
                trans.abort();
                $state.go('app.anon.login');
            } else {
                console.log('You are allowed.');                
            }
        });
    }

})();