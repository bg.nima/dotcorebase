(function () {
    "use strict";
    
    angular
        .module('nwise.infra')
        .provider('jwtToken', jwtTokenProvider);
    
    jwtTokenProvider.$inject = [];
    function jwtTokenProvider() {

        var config = {
            name: 'token',
            options: {
                secure: false
            }
        };
        
        return {
            configure: configure,
            $get: ['$cookies', function ($cookies) {
                return {
                    removeToken: removeToken,
                    setToken: setToken,
                    getToken: getToken,
                    getAuthorizationHeader: getAuthorizationHeader
                }

                function setToken(token) {
                    $cookies.put(config.name, token, config.options);
                }
                
                function getToken() {
                    var token = $cookies.get(config.name);
                    return token;
                }
                
                function getAuthorizationHeader() {
                    return 'Bearer ' + getToken();
                }
                
                function removeToken() {
                    $cookies.remove(config.name);
                }

            }]
            
        }

        function configure(params) {
            // Check if is an `object`.
            if (!(params instanceof Object)) {
                throw new TypeError('Invalid argument: `config` must be an `Object`.');
            }

            // Extend default configuration.
            angular.extend(config, params);

            return config;
        }

    }
})();