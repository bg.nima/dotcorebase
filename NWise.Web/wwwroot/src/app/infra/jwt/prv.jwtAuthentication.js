(function () {
    "use strict";

    angular
        .module('nwise.infra')
        .provider('jwtAuthentication', jwtAuthentication);

    jwtAuthentication.$inject = [];
    function jwtAuthentication() {
        return {
            $get: ['jwtToken', function (jwtToken) {
                return {
                    isAuthenticated: isAuthenticated,
                    authenticate: authenticate,
                    unauthenticate: unauthenticate
                }

                function authenticate(token) {
                    jwtToken.setToken(token);
                }

                function unauthenticate() {
                    jwtToken.removeToken();
                }

                function isAuthenticated() {
                    var token = jwtToken.getToken();
                    if (!token) return false;
                    var tokenParts = _parseToken(token);
                    if (tokenParts[1].exp && (new Date(tokenParts[1].exp * 1000)) < (new Date())) {
                        return false;
                    }
                    return true;
                }


                function _parseToken(token) {
                    var base64Parts = token.split('.');
                    var tokenParts = [];
                    for (var i = 0; i < 2; i++) {
                        var base64 = base64Parts[i] ;
                        tokenParts.push(JSON.parse(window.atob(base64)))
                    }
                    return tokenParts;
                }
            }]
        }

    }
})();