(function () {
  "use strict";
  
  angular
      .module('nwise.infra')
      .config(nwConfig);
  
  nwConfig.$inject = ['$stateProvider'];
  function nwConfig($stateProvider) {

      $stateProvider.state({
          name: 'app.anon.login',
          url: '/signin',
          templateUrl: 'app/infra/pages/membership/tmpl.signin.html',
          controller: 'SigninPageController',
          controllerAs: 'vm',
          cssClass: 'login-page',
          anonymouse: true
      }).state({
          name: 'app.anon.forgot',
          url: '/forgot',
          templateUrl: 'app/infra/pages/membership/tmpl.forgot.html',
          cssClass: 'fp-page',
          anonymouse: true          
      }).state({
          name: 'app.anon.signup',
          url: '/signup',
          templateUrl: 'app/infra/pages/membership/tmpl.signup.html',
          cssClass: 'signup-page',
          controller: 'SignupPageController',
          controllerAs: 'vm',          
          anonymouse: true          
      });
      
  }
})();