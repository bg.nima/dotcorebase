(function() {
    'use strict';

    angular
        .module('nwise.infra')
        .controller('SigninPageController', SigninPageController);

    SigninPageController.$inject = ['authenticationService', '$state', 'notificationService'];
    function SigninPageController(authenticationService, $state, notificationService) {
        var vm = this;
        vm.username = '';
        vm.password = '';
        vm.signin = signin;

        activate();

        ////////////////

        function activate() { }

        function signin() {
            authenticationService.signin(vm.username, vm.password).then(function (result) {
                $state.go('app.auth.dashboard');
            }, function() {
                notificationService.toastDanger('Invalid username/password.', 'Error');
            })
        }
    }
})();