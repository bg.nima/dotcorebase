(function () {
    'use strict';

    angular
        .module('nwise.infra')
        .factory('httpService', httpService);

    httpService.$inject = ['$http', '$q', 'config'];
    function httpService($http, $q, configSrv) {
        var service = {
            $get: $get,
            $post: $post,
            $put: $put,
            $delete: $delete
        };

        return service;

        function httpRequest(type, url, data, config) {
            var params = [];
            data = data || '';
            config = config || {};
            config = angular.merge({}, config, {
                headers : {
                    'Content-Type': 'application/json'
                }
            });

            var deferred = $q.defer();
            var baseUrl = configSrv.getServerBaseUrl();
            params.push( baseUrl + url);
            if (['put', 'post'].indexOf(type)>-1) {
                params.push(data);
            } else if (['delete'].indexOf(type)>-1) {
                config.data = data;
            }
            params.push(config);


            $http[type].apply(null, params).then(function(response){
                if (response.status>=200 && response.status<=399){
                    deferred.resolve(response.data);
                } else {
                    deferred.reject(response);
                }
            },function (response) {
                deferred.reject(response);
            });

            return deferred.promise;
        }


        function $get(url, config) {
            return httpRequest('get', url, config);
        }

        function $post(url, data, config) {
            return httpRequest('post', url, data, config);
        }

        function $put(url, data, config) {
            return httpRequest('put', url, data, config);
        }

        function $delete(url, data, config) {
            return httpRequest('delete', url, data, config);
        }

    }
})();
