(function() {
    'use strict';

    angular
        .module('nwise.infra')
        .factory('authenticationService', authenticationService);

    authenticationService.$inject = ['jwtAuthentication', 'httpService', '$state'];
    function authenticationService(jwtAuthentication, httpService, $state) {
        var service = {
            signin: signin,
            signup: signup,
            signout: signout
        };
        
        return service;

        ////////////////
        
        function signup(userVm) {
            return httpService.$post('api/auth/signup', userVm);
        }
        
        function signin(username, password) { 
            return httpService.$post('api/auth/signin', {
                username: username,
                password: password
            }).then(function(result) {
                jwtAuthentication.authenticate(result.data.token);
                return result;
            });
        }

        function signout() {
            jwtAuthentication.unauthenticate();
            $state.go('app.anon.login');
        }
    }
})();