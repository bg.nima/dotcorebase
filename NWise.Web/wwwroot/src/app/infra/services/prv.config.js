(function () {
    'use strict';

    angular
        .module('nwise.infra')
        .provider('config', configProvider);

    configProvider.$inject = [];

    function configProvider() {
        var serverBaseUrl,
            appBaseName,
            appAbbreviation,
            allowAnonymouseRegister = false,
            appTitle,
            hasLoginCaptcha,
            colorfullLogoSrc;

        return {
            setAllowAnonymouseRegister: function (val) {
                allowAnonymouseRegister = val;
            },
            setServerBaseUrl: function (val) {
                serverBaseUrl = val;
            },
            setAppTitle: function (val) {
                appTitle = val;
            },
            setHasLoginCaptcha: function (val) {
                hasLoginCaptcha = val;
            },
            setColorfullLogoSrc: function (val) {
                colorfullLogoSrc = val;
            },
            setAppBaseName: function (val) {
                appBaseName = val;
            },
            setAppAbbreviation: function (abbr) {
                appAbbreviation = abbr;
            },
            $get: function () {
                return {
                    getServerBaseUrl: getServerBaseUrl,
                    getAppAbbreviation: getAppAbbreviation,
                    appTitle: appTitle,
                    allowAnonymouseRegister: allowAnonymouseRegister,
                    hasLoginCaptcha: hasLoginCaptcha,
                    colorfullLogoSrc: colorfullLogoSrc,
                    getApplicationBaseName: getApplicationBaseName
                }
            }
        }



        function getAppAbbreviation() {
            if (typeof(appAbbreviation)==='undefined') throw Error('application name abbreviation is not set yet!');
            return appAbbreviation;
        }

        function getApplicationBaseName() {
            if (typeof(appBaseName)==='undefined') throw Error('application base name is not set yet!');
            return appBaseName;
        }

        function getServerBaseUrl() {
            if (typeof(serverBaseUrl)==='undefined') throw Error('server base url is not defined');
            return serverBaseUrl;
        }
    }
})();