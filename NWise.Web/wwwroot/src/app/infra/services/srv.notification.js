(function () {
	'use strict';

	angular
		.module('nwise.infra')
		.factory('notificationService', notificationService);

	notificationService.$inject = ['$q'];

	function notificationService($q) {
		var service = {
			alertSuccess: alertSuccess,
			alertWarning: alertWarning,
			alertError: alertError,
			confirm: confirm,
			prompt: prompt,
			showModal: showModal,
			toastSuccess: toastSuccess,
			toastWarning: toastWarning,
			toastInfo: toastInfo,
			toastDanger: toastDanger
		};


		return service;

		////////////////



		function toastSuccess(message, title, options) {
			toast(message, title, {colorName: 'success'});
		}

		function toastWarning(message, title, options) {
			toast(message, title, {colorName: 'warning'});
		}

		function toastInfo(message, title, options) {
			toast(message, title, {colorName: 'info'});
		}

		function toastDanger(message, title, options) {
			toast(message, title, {colorName: 'danger'});
        }
        
        function toast(message, title, opts) {
            
            var options = {
                colorName: 'success',
                allowDismiss: true,
                timer:2000
            };
            angular.extend(options, opts);
            $.notify({
                message: message,
                title: title
            },
            {
                type: 'alert-' + options.colorName,
                allow_dismiss: options.allowDismiss,
                newest_on_top: true,
                timer: options.timer,
                placement: {
                    from: 'top',
                    align: 'right'
                },
                // animate: {
                //     enter: animateEnter,
                //     exit: animateExit
                // },
                template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (options.allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
            });            
        }


		/**
		 * shows bootstrap modal using angular ui services
		 * @param {any} options
		 * @returns uibModal service result
		 */
		function showModal(options) {
			return $uibModal.open(options).result;
		}


		/**
		 * Show confirmation message
		 * @param  {string} title (Message title)
		 * @param  {string} text (Message text)
		 * @param  {string} cbtnText (Confirm button text)
		 * @param  {string} rbtnText (Reject button text)
		 * @returns {Promise}
		 */
		function confirm(title, text, cbtnText, rbtnText) {
			return sweetalert(title, text, 'warning', {
				showCancelButton: true,
				confirmButtonText: cbtnText,
				cancelButtonText: rbtnText
			});
		}

		/**
		 * Shows prompt message and returns a {promise}
		 * 
		 * @param {string} title Prompt title
		 * @param {string} text Prompt body message
		 */
		function prompt(title, text) {
			var deferred = $q.defer();
			swal({
					title: title,
					text: text,
					type: "input",
					showCancelButton: true,
					closeOnConfirm: true,
					animation: "slide-from-top",
					inputPlaceholder: ""
				},
				function (inputValue) {
					if (inputValue === false) {
						deferred.reject();
						return false;
					}

					if (inputValue === "") {
						swal.showInputError("You need to write something!");
						return false
					}

					deferred.resolve(inputValue);
				});
			return deferred.promise;
		}

		function alertWarning(title, text) {
			return sweetalert(title, text, 'warning');
		}

		function alertError(title, text) {
			return sweetalert(title, text, 'error');
		}

		function alertSuccess(title, text) {
			return sweetalert(title, text, 'success');
		}

		function sweetalert(title, text, type, config) {
			config = config || {};
			var deferred = $q.defer();

			utilityService.translate([title, text, config.cancelButtonText, config.confirmButtonText]).then(function (trans) {
				swal({
					title: trans[title],
					text: trans[text],
					type: type,
					showCancelButton: config.showCancelButton,
					confirmButtonText: trans[config.confirmButtonText],
					cancelButtonText: trans[config.cancelButtonText]
				}, function (result) {
					if (result)
						deferred.resolve(true);
					else
						deferred.reject(false);
				});
			});

			return deferred.promise;
		}
	}
})();