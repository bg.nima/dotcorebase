(function () {
    "use strict";
    
    angular
        .module("nwise")
        .controller("AppController", AppController);
     
    AppController.$inject = ['$rootScope', '$state','$transitions'];
    function AppController($rootScope, $state,$transitions) {
        activate();

        function activate() {
            $transitions.onBefore({}, function (trans) {
                $rootScope.bodyCssClass = trans.to().cssClass;
            });
            $rootScope.bodyCssClass = $state.current.cssClass;
        }
    }
})();