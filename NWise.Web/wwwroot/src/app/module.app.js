(function () {
    "use strict";

    angular.module('nwise',
        [
            'nwise.infra',
            'ui.router',
            'ngCookies'
        ]);
})();