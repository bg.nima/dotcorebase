(function () {
    angular
        .module('nwise')
        .config(nwiseConfig);
    
    nwiseConfig.$inject = ['$stateProvider','$urlRouterProvider','$locationProvider'];
    function nwiseConfig($stateProvider,$urlRouterProvider,$locationProvider) {
        
        $urlRouterProvider.otherwise("/dashboard");
        $locationProvider.html5Mode(true);
        $stateProvider.state({
            name: 'app',
            url: '',
            abstract: true,
            controller: 'AppController',
            template: '<ui-view></ui-view>'
        }).state({
            name: 'app.anon',
            url: '',
            abstract: true,
            template: '<ui-view></ui-view>'
        }).state({
            name: 'app.auth',
            url: '',
            abstract: true,
            templateUrl: 'app/tmpl.app-auth.html'
        }).state({
            name: 'app.auth.dashboard',
            url: '/dashboard',
            templateUrl: 'app/pages/dashboard/tmpl.dashboard.html'
        });
        
    }
})();