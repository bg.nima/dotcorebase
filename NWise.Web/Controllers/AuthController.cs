using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NWise.Model.View;
using NWise.Service;
using NWise.Web.Infra;

namespace NWise.Web.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : BaseController
    {
        private readonly IConfiguration configuration;
        private readonly IUserService userService;

        public AuthController(IConfiguration configuration, IUserService userService)
        {
            this.configuration = configuration;
            this.userService = userService;
        }

        [HttpPost]
        [Route("signin")]
        [AllowAnonymous]
        public async Task<IActionResult> Signin([FromBody] SigninViewModel login)
        {
            if (!ModelState.IsValid)
            {
                return new BadResponse("Username/Password not provided");
            }

            try
            {
                var permissions = await userService.Authenticate(login.Username, login.Password);
                var claims = new List<Claim>()
                {
                    new Claim(JwtRegisteredClaimNames.Sub, login.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                permissions.ForEach(p => 
                {
                    claims.Add(new Claim(ClaimTypes.Role, p));
                });

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(configuration["Tokens:Issuer"],
                    configuration["Tokens:Issuer"],
                    claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: cred);

                return new OkResponse(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }
            catch (Exception)
            {
                return new BadResponse("Username/Password is not correct");
            }
        }

        [HttpPost]
        [Route("signup")]
        public async Task<IActionResult> Signup([FromBody] CreateUserViewModel model) 
        {
            if (!ModelState.IsValid) 
            {
                return new BadResponse("validation", ModelState.Keys);
            }
            
            try
            {
                model.RoleId = 3;
                var id = await userService.CreateAsync(model);
                return new OkResponse(id);
            }
            catch (Exception ex)
            {
                return new BadResponse(ex.Message);
            }
        }

        [HttpGet]
        [Authorize(Roles="user.create")]
        [Route("me")]
        public IActionResult Me() 
        {
            return Ok("Nima");
        }
    }
}