
using System.Threading.Tasks;
using NWise.Model.Db;
using NWise.Model.View;

namespace NWise.Service
{
    public interface IGenericService<TEntity> where TEntity: class, IEntity 
    {
        Task<int> Create(GenericCreateViewModel<TEntity> model);    
        Task<int> Update(GenericUpdateViewModel<TEntity> model);
        Task<int> Delete(int id);
    }
}