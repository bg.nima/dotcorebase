namespace NWise.Service
{
    public interface IUtilityService
    {
        string HashPassword(string password);
    }
}