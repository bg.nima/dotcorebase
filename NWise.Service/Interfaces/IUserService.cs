using System.Collections.Generic;
using System.Threading.Tasks;
using NWise.Model.Db;
using NWise.Model.View;

namespace NWise.Service
{
    public interface IUserService : IGenericService<User>
    {
        /// <summary>
        /// Check user validity in database
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Raw password (un-hashed)</param>
        /// <returns>Returns list of permissions if authenticated</returns>
        Task<List<string>> Authenticate(string username, string password);

        Task<long> CreateAsync(CreateUserViewModel model);
    }
}