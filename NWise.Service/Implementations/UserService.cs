using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NWise.Data;
using NWise.Model.Db;
using NWise.Model.View;

namespace NWise.Service
{
    public class UserService : GenericService<User>, IUserService
    {
        private readonly IUtilityService utilityService;

        public UserService(DotNetDbContext dbContext, IMapper mapper, IUtilityService utilityService) : base(dbContext, mapper)
        {
            this.utilityService = utilityService;
        }

        public async Task<List<string>> Authenticate(string username, string password)
        {
            var hashedPassword = utilityService.HashPassword(password);

            var permissions = await (from user in dbContext.Users
                                    join rolePermission in dbContext.RolePermissions on user.RoleId equals rolePermission.RoleId
                                    join permission in dbContext.Permissions on rolePermission.PermissionId equals permission.Id
                                    where user.Username.ToLower() == username.ToLower() && user.Password == hashedPassword
                                    select permission.Code).ToListAsync();

            if (!permissions.Any()) {
                throw new Exception("Username/Password not valid!");
            }
            
            return permissions;
        }

        public async Task<long> CreateAsync(CreateUserViewModel model) 
        {
            var user = mapper.Map<User>(model);
            user.CreatedAt = DateTime.Now;
            user.Password = utilityService.HashPassword(model.Password);
            dbContext.Users.Add(user);
            await dbContext.SaveChangesAsync();
            return user.Id;
        }
    }
}