using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NWise.Data;
using NWise.Model.Db;
using NWise.Model.View;

namespace NWise.Service
{
    public static class DependancyHelper 
    {
        public static IServiceCollection ResolveServiceLayerDependancies(this IServiceCollection services) 
        {
            var mapper = ConfigureMapper();
            return services.AddScoped<DotNetDbContext>()
                           .AddSingleton(mapper)
                           .AddScoped<IUtilityService, UtilityService>()
                           .AddScoped<IUserService, UserService>();
        }

        private static IMapper ConfigureMapper() 
        {
            var config = new AutoMapper.MapperConfiguration(cfg => 
            {
                cfg.CreateMap<CreateUserViewModel, User>();
            });

            return config.CreateMapper();
        }
    }
    
}