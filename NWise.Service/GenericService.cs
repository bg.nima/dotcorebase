
using System.Threading.Tasks;
using NWise.Model.Db;
using NWise.Model.View;
using NWise.Data;
using AutoMapper;

namespace NWise.Service
{
    public class GenericService<TEntity> : IGenericService<TEntity> where TEntity : class, IEntity
    {
        protected readonly DotNetDbContext dbContext;
        protected readonly IMapper mapper;

        protected GenericService(DotNetDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }
        
        public async Task<int> Create(GenericCreateViewModel<TEntity> model)
        {
            throw new System.NotImplementedException();
        }

        public async Task<int> Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<int> Update(GenericUpdateViewModel<TEntity> model)
        {
            throw new System.NotImplementedException();
        }
    }
}